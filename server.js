const http = require("http")
const port = 4000;

const server = http.createServer((req, res) => {
    if(req.url === "/htmlDemo"){
        res.setHeader("Content-type", "text/html");
        res.write(`
        <h1>Html response demo</h1>
        <h2>
            hello world
        </h2>`)
        res.end();
    } else {
        res.write("unknown url!")
        res.end();
    }

    
    console.log(req);
    // res.write("hello world!")
})


server.listen(port, () => {
     console.log(`listening on port ${port}`)
});

